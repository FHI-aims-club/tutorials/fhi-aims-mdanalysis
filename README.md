# FHI-aims and MDAnalysis

This is a short tutorial to outline the interaction between the MDAnaysis suite of tools and the FHI-aims code. MDAnalysis provides tools to analyze molecular dynamics trajectories. For example, one could use it to create a limited snapshot of a region cut out from a larger, classical molecular dynamics trajectory. Please find this web page here:

https://fhi-aims-club.gitlab.io/tutorials/fhi-aims-mdanalysis
