#!/usr/bin/env python
# Copyright (c) 2023 Oliver Beckstein
# Published under the GNU General Public License, version 2 or any later version
# (MDAnalysis is currently published under GPL so any code including it must also
# be under GPL; this will change in the future when MDAnalysis will be under LGPL v2.1+)
#
# extract_all.py: see https://fhi-aims-club.gitlab.io/tutorials/fhi-aims-mdanalysis/1-periodic-system/

import warnings
import MDAnalysis as mda
from MDAnalysis import transformations

u = mda.Universe("./files/MD/NPT/md.tpr", "./files/MD/NPT/md.trr")
u.atoms.names = u.atoms.elements
water = u.atoms.select_atoms("resname SOL")
solute = u.atoms.select_atoms("resname ACE LYSN NME")
assert u.atoms == solute + water

workflow = [transformations.unwrap(u.atoms),
            transformations.center_in_box(solute, center='geometry'),
            transformations.wrap(water, compound='atoms')]
u.trajectory.add_transformations(*workflow)

u.trajectory[-1]

u.atoms.write("geometry.in")
with warnings.catch_warnings():
    warnings.simplefilter("ignore", UserWarning)
    u.atoms.write("geometry.pdb")

