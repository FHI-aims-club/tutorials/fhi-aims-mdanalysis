#!/usr/bin/env python

import MDAnalysis as mda

TPR = "02_emin.tpr"
GRO = "02_emin.gro"

AIMSIN = "geometry.in"
PDB = "geometry.pdb"

u = mda.Universe(TPR, GRO)
u.atoms.names = u.atoms.elements

u.atoms.write(AIMSIN)
u.atoms.write(PDB, remarks="energy minimized with GROMACS")

