#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2023 Oliver Beckstein
# Published under the GNU General Public License, version 2 or any later version
# (MDAnalysis is currently published under GPL so any code including it must also
# be under GPL; this will change in the future when MDAnalysis will be under LGPL v2.1+)
#
# extract_reduced_pbc.py: see https://fhi-aims-club.gitlab.io/tutorials/fhi-aims-mdanalysis/2-periodic-sub-system/

import warnings
import numpy as np
import MDAnalysis as mda
from MDAnalysis import transformations

u = mda.Universe("./files/MD/NPT/md.tpr", "./files/MD/NPT/md.trr")
u.atoms.names = u.atoms.elements
water = u.atoms.select_atoms("resname SOL")
solute = u.atoms.select_atoms("resname ACE LYSN NME")
assert u.atoms == solute + water

workflow = [transformations.unwrap(u.atoms),
            transformations.center_in_box(solute, center='geometry'),
            transformations.wrap(water, compound='atoms')]
u.trajectory.add_transformations(*workflow)

u.trajectory[-1]

# bounding box of the peptide
bbox = solute.bbox()
A, B, C = bbox[1] - bbox[0]
print("unit cell:   a={uc[0]:7.4f} Å  b={uc[1]:7.4f} Å  c={uc[2]:7.4f} Å  ⍺={uc[3]}° β={uc[4]}° ɣ={uc[5]}° ".format(uc=u.dimensions))
print(f"solute bbox: A={A:7.4f} Å  B={B:7.4f} Å  C={C:7.4f} Å")

# additional distance in all directions
delta = 5
rmin = bbox[0] - delta
rmax = bbox[1] + delta

# select complete molecules with "byres" inside the extended box around
# the solute
water_box = water.select_atoms(f"byres prop x > {rmin[0]} and prop x < {rmax[0]} and "
                               f"prop y > {rmin[1]} and prop y < {rmax[1]} and "
                               f"prop z > {rmin[2]} and prop z < {rmax[2]}")
# combined system
smaller_system = solute + water_box

# new unit cell: A, B, C, alpha, beta, gamma
newbox = np.concatenate([rmax - rmin, [90, 90, 90]])

# Put all atoms inside the new unit cell
u.atoms.translate(-rmin)
smaller_system.wrap(compound="atoms", box=newbox, inplace=True)

smaller_system.dimensions = newbox

smaller_system.write("geometry.in")
with warnings.catch_warnings():
    # write PDB output without the needless warnings about defaults for the PDB file
    warnings.simplefilter("ignore", UserWarning)
    smaller_system.write("geometry.pdb")
